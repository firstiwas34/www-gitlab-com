(function($, Vue, VueSelect, Clipboard, module) {
  Vue.component('v-select', VueSelect.VueSelect);

  const salaryCalculatorSelect = {
    props: {
      disabled: { type: Boolean, default: false },
      clearable: { type: Boolean, default: false },
      searchable: { type: Boolean, default: false },
      options: { type: Array, required: true },
      label: { type: String, required: true },
      titleField: { type: String, required: true },
      optionValue: { type: Function, required: false },
      value: { default: null } // wrapper for v-model
    },
    template: '' +
      '<v-select :placeholder="label" :disabled="disabled" :clearable="clearable" :options="options" :searchable="searchable" :label="titleField" :value="value" @input="$emit(\'input\', $event)">' +
      '  <template slot-scope="option" slot="selected-option">' +
      '    <span class="label">' +
      '      {{ label }}' +
      '    </span>' +
      '    <span class="title">' +
      '      {{ option[titleField] }}' +
      '    </span>' +
      '    <span class="subtitle" v-if="optionValue">' +
      '      {{ optionValue(option) }}' +
      '    </span>' +
      '  </template>' +
      '  <template slot-scope="option" slot="option">' +
      '    <span class="key">' +
      '      {{ option[titleField] }}' +
      '    </span>' +
      '    <span class="value" v-if="optionValue">' +
      '      {{ optionValue(option) }}' +
      '    </span>' +
      '  </template>' +
      '</v-select>'
  };

  const defaultLevel = { title: 'N/A', factor: 1 };

  const urlParams = [{
    param: 'experience',
    prop: 'currentExperience',
    findIn: 'experienceFactors',
    findBy: 'label'
  }, {
    param: 'level',
    prop: 'currentLevel',
    findIn: 'roleLevels',
    findBy: 'title',
    default: defaultLevel
  }, {
    param: 'country',
    prop: 'currentCountry',
    findIn: 'allAreas',
    findBy: 'country',
    key: 'country'
  }, {
    param: 'area',
    prop: 'currentArea',
    findIn: 'areas',
    findBy: 'area'
  }];

  module.exports = new Vue({
    el: '.salary-container',
    components: {
      'salary-calculator-select': salaryCalculatorSelect
    },
    data: function() {
      return {
        initialRole: null,
        sourceData: {},
        allRoles: [],
        countries: [],
        allAreas: [],
        countryCurrencies: {},
        experienceFactors: [
          { min: 0.9, max: 1.1, label: 'Experience range' },
          { min: 0.9, max: 0.949, label: 'Learning the role' },
          { min: 0.95, max: 0.999, label: 'Growing in the role' },
          { min: 1.0, max: 1.049, label: 'Thriving in the role' },
          { min: 1.05, max: 1.1, label: 'Expert in the role' }
        ],
        currentRole: null,
        currentLevel: null,
        currentExperience: null,
        currentCountry: null,
        currentArea: null
      };
    },
    beforeMount: function() {
      this.initialRole = this.$el.dataset.role;
    },
    mounted: function() {
      new Clipboard('.copy-me'); // eslint-disable-line no-new
      this.getSourceData();
      this.currentExperience = this.experienceFactors[0];
    },
    watch: {
      currentCountry: function(newCountry) {
        if (this.currentArea && this.currentArea.country !== newCountry) {
          this.currentArea = null;
        }
      },
      currentRole: function(newRole) {
        if (newRole) {
          this.setRoleLevels();
        }
      }
    },
    computed: {
      showRoleSelector: function() {
        return !this.initialRole;
      },
      renderCalculator: function() {
        return this.currentRole && !$.isEmptyObject(this.sourceData);
      },
      currentLevelFactor: function() {
        return this.currentLevel ? this.currentLevel.factor : null;
      },
      currentLocationFactor: function() {
        return this.currentArea ? this.currentArea.locationFactor : null;
      },
      roleLevels: function() {
        if (!this.currentRole) { return null; }

        return this.sourceData.roleLevels[this.currentRole.levels] || this.currentRole.levels;
      },
      contractType: function() {
        if (!this.currentCountry) { return null; }

        return this.findByCountry(this.sourceData.contractTypes, this.currentCountry);
      },
      contractTypeFactor: function() {
        if (!this.contractType) { return null; }

        return this.contractType.employee_factor || this.contractType.contractor_factor;
      },
      areas: function() {
        const currentCountry = this.currentCountry;

        return this.allAreas.filter(function(location) {
          return location.country === currentCountry;
        });
      },
      calculateCompensation: function() {
        return this.currentLevel &&
          !$.isEmptyObject(this.currentExperience) &&
          this.currentArea &&
          this.contractType;
      },
      compensationRange: function() {
        if (!this.calculateCompensation) {
          return '--';
        }

        return this.formatAmount(this.calculateSalary(this.currentExperience.min)) +
          ' - ' +
          this.formatAmount(this.calculateSalary(this.currentExperience.max));
      },
      localCurrencyRange: function() {
        if (!this.calculateCompensation) { return null; }

        const currency = this.countryCurrencies[this.currentArea.country];

        if (!currency) { return null; }

        return this.formatAmount(this.calculateSalary(this.currentExperience.min, currency.rate), currency.code) +
          ' - ' +
          this.formatAmount(this.calculateSalary(this.currentExperience.max, currency.rate), currency.code);
      },
      canHireCountry: function() {
        const currentCountry = this.currentCountry;

        return !this.sourceData.countryNoHire.find(function(noHire) {
          return noHire === currentCountry;
        });
      },
      link: function() {
        const vue = this;
        const url = new URL(window.location.href);

        urlParams.forEach(function(param) {
          const value = vue[param.prop];

          url.searchParams.set(param.param, value[param.findBy] || value);
        });

        url.hash = '#compensation';

        return url.toString();
      }
    },
    methods: {
      getSourceData: function() {
        const vue = this;

        $.get('/salary/data.json').then(function(data) {
          vue.sourceData = data;

          // countries
          vue.countries = data.locationFactors.map(function(location) {
            return location.country;
          }).filter(function(value, index, self) {
            return self.indexOf(value) === index;
          }).sort();

          // areas
          vue.allAreas = data.locationFactors.map(function(location) {
            location.locationFactor = location.locationFactor * 0.01;

            return location;
          }).sort(function(location) {
            return location.area;
          });

          // country -> currency mapping
          data.currencyExchangeRates.rates_to_usd.forEach(function(currency) {
            currency.countries.forEach(function(country) {
              vue.countryCurrencies[country] = {
                code: currency.currency_code,
                rate: currency.rate
              };
            });
          });

          vue.allRoles = data.roles;

          vue.currentRole = data.roles.find(function(role) {
            return role.title === vue.initialRole;
          });

          // Let watchers run first
          vue.$nextTick(function() {
            if (!this.showRoleSelector) {
              vue.setParamsFromUrl(window.location.href);
            }
          });
        });
      },
      setRoleLevels: function() {
        if (this.roleLevels) {
          this.currentLevel = this.roleLevels.find(function(level) {
            return level.is_default;
          });
        } else {
          this.currentLevel = defaultLevel;
        }
      },
      setParamsFromUrl: function(currentUrl) {
        const vue = this;
        const currentParams = (new URL(currentUrl)).searchParams;

        urlParams.forEach(function(param) {
          const urlParam = currentParams.get(param.param);

          if (!urlParam) { return; }

          let foundParam = (vue[param.findIn] || []).find(function(item) {
            return item[param.findBy] === urlParam;
          }) || param.default;

          if (!foundParam) { return; }

          if (param.key) { foundParam = foundParam[param.key]; }

          vue[param.prop] = foundParam;
        });
      },
      formatAmount: function(amount, currencyCode) {
        const formattedAmount = amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');

        if (currencyCode) {
          return formattedAmount + ' ' + currencyCode;
        }

        return '$' + formattedAmount;
      },
      formatRoleLevel: function(roleLevel) {
        return roleLevel.factor;
      },
      formatExperienceFactor: function(experienceFactor) {
        return experienceFactor.min + ' to ' + experienceFactor.max;
      },
      formatAreaLocationFactor: function(area) {
        return area ? area.locationFactor.toFixed(3) : '--';
      },
      findByCountry: function(data, country) {
        const vue = this;
        const fallback = function() {
          if (country === '*') { return null; }

          return vue.findByCountry(data, '*');
        };

        return data.find(function(item) { return item.country === country; }) || fallback();
      },
      calculateSalary: function(experienceFactor, currencyRate) {
        if (!this.calculateCompensation) { return null; }

        return Math.round(
          this.currentRole.salary *
            this.currentLocationFactor *
            this.currentLevelFactor *
            experienceFactor *
            this.contractTypeFactor /
            (currencyRate || 1)
        );
      }
    }
  });
})(window.$, window.Vue, window.VueSelect, window.Clipboard, typeof module !== 'undefined' ? module : {});
