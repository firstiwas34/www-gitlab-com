---
layout: markdown_page
title: "People Operations Planning"
---

People Ops will manage a monthly PeopleOps calendar to highlight important dates.  Examples would include Summit dates, experience factor worksheet dates, merit or compensation program dates, manager and team member training, etc.  

##  Remaining 2018 People Operations Planning
- **October** - Engagement Survey via Cultureamp
- **October 25th**  - New Experience Factor Worksheet Training for Managers and Team members
- **November 15th** - Annual Experience Factor Worksheet due to People Ops Analyst 
- **November** - Manager Training (TBD)
- **December** - Manager Training (TBD)

## 2019 People Operations Planning
- **January 1st** - Annual compensation adjustments effective
- **January 1st** - Exchange Rate Review
- **January 16th** - 360 kick off via Cultureamp
- **January** - Manager Training (TBD)
- **February** - Manager Training (TBD)
- **March** - Manager Training (TBD)
- **April** - Manager Training (TBD)
- **May** - Manager Training (TBD)
- **May** - Summit
- **June** - Manager Training (TBD)
- **July 1st** - Exchange Rate Review
- **July 17th** - 360 Kick off via Cultureamp
- **July** - Manager Training (TBD)
- **August** - Manager Training (TBD)
- **September** - Manager Training (TBD)
- **August/September** - Open Enrollement (TBD) 
- **October** - Experience Factor Worksheets and Annual Compensation Review (TBD) 
- **October 8th** - Engagement Survey via Cultureamp
- **October 16th** - Experience Factor Worksheet Training for Managers and Team members
- **November** - Annual Experience Factor Worksheet due to People ops analyst
- **November** - Manager Training (TBD)
- **December** - Manager Training (TBD)
